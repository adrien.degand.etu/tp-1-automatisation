FROM maven:3.8.3-openjdk-17
COPY . .
RUN mvn install -Dmaven.test.skip=true
RUN java -jar GetThingsDone-app/target/GetThingsDone-app-1.0-SNAPSHOT.jar